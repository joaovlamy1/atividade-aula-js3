function intersection(...lista) {
    let resultado = [...lista[0]];

    for (let i=0; i < lista.length; i++) {
        resultado = resultado.filter(comum => lista[i].includes(comum));
    }
    
    return resultado;
}

console.log(intersection([1, 2, 3], [3, 3, 7,1], [9, 111, 3,1]));
console.log(intersection([120, 120, 110, 2], [110, 2, 130]));
