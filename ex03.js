function filtra(filtro, lista) {
    return lista.filter(str => {
        for (const chr of str) {
            if (!filtro.includes(chr)) {
                return false;
            }
        }
        return true;
    });
}

console.log(filtra("ab", ["abc", "ba", "ab", "bb", "kb"]));
console.log(filtra("pkt", ["pkt", "pp", "pata", "po", "kkkkkkk"]));
